﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace trim_bin_tool
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly mainAssembly = Assembly.GetEntryAssembly();
            AssemblyCompanyAttribute companyAttribute = (AssemblyCompanyAttribute)GetAttribute(mainAssembly, typeof(AssemblyCompanyAttribute));
            AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)GetAttribute(mainAssembly, typeof(AssemblyTitleAttribute));
            AssemblyVersionAttribute versionAttribute = (AssemblyVersionAttribute)GetAttribute(mainAssembly, typeof(AssemblyVersionAttribute));
            string appTitle = titleAttribute != null ? titleAttribute.Title : mainAssembly.GetName().Name;
            string appCompany = companyAttribute != null ? companyAttribute.Company : "";
            string appVersion = versionAttribute != null ? versionAttribute.Version : mainAssembly.GetName().Version.ToString();

            if (args.Length != 1 && args.Length != 3)
                error(appTitle, appVersion, appCompany);

            if ((args.Length == 1) && (args[0].ToLower() == "-version"))
                Console.WriteLine("{0} {1}, written by {2} for catalinnc\n", appTitle, appVersion, appCompany);

            else if (args.Length == 3)
            {
                string positionStr = args[0].ToLower();
                long? position = null;

                string valueStr = args[1].ToLower();
                long? value = null;

                string filename = args[2];

                // Let's make sure that the file exists before we proceed...
                if (!File.Exists(filename))
                    error(appTitle, appVersion, appCompany);

                FileInfo fi = new FileInfo(filename);

                // Work out the value
                if (valueStr.Contains("0x"))
                    value = HexToLong(valueStr);
                else
                {
                    long tmp;
                    if (!long.TryParse(valueStr, out tmp))
                        error(appTitle, appVersion, appCompany);
                    value = tmp;
                }
                if (!value.HasValue)
                    error(appTitle, appVersion, appCompany);

                // Work out the position
                if (positionStr == "start")
                    position = 0;
                else if (positionStr == "end")
                {
                    long byteCount = 0;

                    if (valueStr.Contains("0x"))
                    {
                        byteCount = valueStr.Substring(2).Length / 2;

                        if (valueStr.Substring(2).Length % 2 != 0)
                            byteCount++;

                        if (byteCount < 2)
                            byteCount = 2;

                        value -= 2;
                    }
                    else
                    {
                        byte[] tmp = BitConverter.GetBytes(value.Value);

                        for (int i = tmp.Length - 1; i > 0; i--)
                            if (tmp[i] != 0)
                            {
                                byteCount = ++i;
                                break;
                            }
                    }

                    position = fi.Length - byteCount;
                }
                if (!position.HasValue)
                    error(appTitle, appVersion, appCompany);

                // Push the changes
                if (positionStr == "end")
                    using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.RandomAccess))
                        stream.SetLength(position.Value - value.Value);

                if (positionStr == "start")
                {
                    string tmpFile = Path.GetTempFileName();

                    using (FileStream inStream = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.RandomAccess))
                    using (FileStream outStream = new FileStream(tmpFile, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.RandomAccess))
                    {
                        inStream.Position = value.Value;

                        byte[] readBytes = new byte[1024 * 1024 * 8];
                        int bytesReadCount;
                        while ((bytesReadCount = inStream.Read(readBytes, 0, readBytes.Length)) > 0)
                            outStream.Write(readBytes, 0, bytesReadCount);
                    }

                    File.Delete(filename);
                    File.Move(tmpFile, filename);
                }
            }
        }

        private static void error(string appTitle, string appVersion, string appCompany)
        {
            Console.WriteLine("{0} {1}, written by {2} for catalinnc\n", appTitle, appVersion, appCompany);

            Console.WriteLine("Usage: {0} string_position decimal/hexadecimal_value target_file\n", appTitle);

            Console.WriteLine("Valid position strings:\tend\tstart\n");

            Console.WriteLine("Examples: {0} start 0x14 target_file", appTitle);
            Console.WriteLine("\t  {0} endt 50 target_file", appTitle);

            Environment.Exit(1);
        }

        private static long HexToLong(string hexString)
        {
            long value = long.Parse(hexString.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier);

            return value;
        }

        private static Attribute GetAttribute(Assembly assembly, Type attributeType)
        {
            object[] attributes = assembly.GetCustomAttributes(attributeType, false);
            if (attributes.Length == 0)
            {
                return null;
            }
            return (Attribute)attributes[0];
        }
    }
}
